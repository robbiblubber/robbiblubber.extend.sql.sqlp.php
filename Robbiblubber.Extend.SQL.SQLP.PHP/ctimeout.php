<?php namespace Robbiblubber\Extend\SQL\SQLP;

require_once dirname(__FILE__) . '/include/robbiblubber.extend.sql.sqlp/connection.class.php';



header('Content-Type: text/plain');



if(!Connection::authenticate())
{
    echo('success=false;message=Authentication failed.;');
}
else
{
    if(isset($_GET['value']))
    {
        if(Connection::setTimeout((int) $_GET['value']))
        {
            echo('success=true;timeout=' . Connection::getTimeout() . ';');
        }
        else 
        {
            echo('success=false;message=Invalid timeout.;');
        }
    }
    else
    {
        echo('success=true;timeout=' . Connection::getTimeout() . ';');
    }
 }
?>