<?php namespace Robbiblubber\Util\Configuration;

require_once dirname(__FILE__) . '/../robbiblubber.util.coding/outputformat.class.php';
require_once dirname(__FILE__) . '/../robbiblubber.util.coding/hex.class.php';
require_once dirname(__FILE__) . '/../robbiblubber.util.coding/base64.class.php';
require_once dirname(__FILE__) . '/../robbiblubber.util.coding/uuencode.class.php';
 
use Robbiblubber\Util\Coding\OutputFormat;
use Robbiblubber\Util\Coding\Hex;
use Robbiblubber\Util\Coding\Base64;
use Robbiblubber\Util\Coding\UUencode;
 
 

/** This class implements a ddp string. */
class DdpString
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members                                                                                                  //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Value array. */
    private $_Values;
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // constructors                                                                                                     //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Creates a new instance of this class.
     * @param string $value 
     * @param int $option Encoding option. */
    public function __construct($value = NULL, $option = OutputFormat::PLAIN)
    {
        $this->_Values = array();        
        
        if($value == NULL) { return;  }
        
        switch($option)
        {
            case OutputFormat::BASE64:
                $value = Base64::toString($value);
                break;
            case OutputFormat::HEX:
                $value = Hex::toString($value);
                break;
            case OutputFormat::UUENCODE:
                $value = UUencode::toString($value);
                break;
        }
        
        $value = DdpString::_maskRead($value);
        
        foreach(split(';', $value) as $i)
        {
            if(trim($i) == '') { continue; }
            
            if(strpos($i, '=') !== FALSE)
            {
                $this->_Values[DdpString::_unmaskStore(trim(substr($i, 0, strpos($i, '='))))] = DdpString::_unmaskStore(trim(substr($i, strpos($i, '=') + 1)));
            }
        }
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public static methods                                                                                            //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Gets a ddp value from a string.
     * @param string $value Value.
     * @param int $option Encoding option.
     * @return DdpString Object. */
    public static function fromString($value, $option = OutputFormat::PLAIN)
    {
        return new DdpString($value, $option);
    }
    
    
    /** Reads a ddp value from a file.
     * @param string $file File name.
     * @param int $option Encoding option.
     * @return DdpString Object. */
    public static function fromFile($file, $option = OutputFormat::PLAIN)
    {
        if(file_exists($file))
        {
            return new DdpString(file_get_contents($file), $option);
        }
        
        return new DdpString();
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public methods                                                                                                   //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Gets the value for a given key.
     * @param string $key Key.
     * @return mixed Value. */
    public function getValue($key)
    {
        return $this->_Values[$key];
    }
    
    
    /** Gets the value for a given key as a Boolean.
     * @param string $key Key.
     * @return boolean Value. */
    public function getBoolean($key)
    {
        if(is_numeric($key))
        {
            return ($key != 0);
        }
        else 
        {
            return ((substring(strtolower($key), 0, 1) == 't') || (substring(strtolower($key), 0, 1) == 'y'));
        }
    }
    
    
    /** Sets the value for a given key.
     * @param string $key Key.
     * @param mixed $value Value. */
    public function setValue($key, $value)
    {
        $this->_Values[$key] = $value;
    }


        /** Returns an arry for this instance.
     * @return array Array. */
    public function toArray()
    {
        $rval = $this->_Values;
        
        return $rval;
    }
    
    
    /** Returns a string representation of this instance.
     * @param int $option Encoding option.
     * @return string String. */
    public function toString($option = OutputFormat::PLAIN)
    {
        $rval = '';
        
        foreach ($this->_Values as $k => $v)
        {
            $rval .= (DdpString::_maskWrite($k) . '=' . DdpString::_maskWrite($v) . ';');
        }
        
        switch($option)
        {
            case OutputFormat::BASE64:
                $rval = Base64::fromString($value);
                break;
            case OutputFormat::HEX:
                $value = Hex::fromString($value);
                break;
            case OutputFormat::UUENCODE:
                $value = UUencode::fromString($value);
                break;
        }
        return $rval;
    }
    
    
    /** Saves the ddp string to file.
     * @param string $file File name.
     * @param int $option Encoding option. */
    public function save($file, $option = OutputFormat::PLAIN)
    {
        file_put_contents($file, $this->toString($option));
    }
    
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public static methods                                                                                            //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Masks a string.
     * @param string $str String.
     * @return string Masked string. */
    public static function mask($str)
    {
        return DdpString::_maskWrite($str);
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private static methods                                                                                           //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Masks a string.
     * @param string $str String.
     * @return string Masked string. */
    private static function _maskRead($str)
    {
        return str_replace("\\=", "\e\eq", str_replace("\\;", "\e\es", 
               str_replace("\\r", "\r", str_replace("\\n", "\n", str_replace("\\t", "\t", str_replace("\\t", "\t", str_replace("\\_", "\e\e_",
               str_replace("\\\\", "\e\eB", $str))))))));
    }
    
    
    /** Masks a string.
     * @param string $str String.
     * @return string Masked string. */
    private static function _maskWrite($str)
    {
        $m = '';        
        while(substr($str, 0, 1) == ' ')
        {
            $m .= "\e";
            $str = substr($str, 1);
        }
        
        $str = ($m . $str);
        
        $m = '';
        while(substr($str, strlen($str) - 1) == ' ')
        {
            $m .= "\e";
            $str = substr($str, 0, strlen($str) - 1);
        }        
        $str = ($str . $m);
        
        return str_replace("\e", "\\_", str_replace("=", "\\=", str_replace(";", "\\;", 
               str_replace("\r", "\\r", str_replace("\n", "\\n", str_replace("\t", "\\t", str_replace("\t", "\\t", str_replace("\\", "\\\\", $str))))))));
    }
    
    
    /** Unmasks a string.
     * @param string $str Masked string.
     * @return string String. */
    private static function _unmaskStore($str)
    {
        return str_replace("\e\e_", " ", str_replace("\e\eq", "=", str_replace("\e\es", ";", str_replace("\e\eB", "\\", $str))));
    }
}

?>