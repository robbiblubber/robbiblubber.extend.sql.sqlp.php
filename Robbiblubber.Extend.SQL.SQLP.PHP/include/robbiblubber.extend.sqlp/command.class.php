<?php namespace Robbiblubber\Extend\SQL\SQLP;

require_once dirname(__FILE__) . '/connection.class.php';
require_once dirname(__FILE__) . '/dbparam.class.php';
require_once dirname(__FILE__) . '/../robbiblubber.util.configuration/ddpstring.class.php';
require_once dirname(__FILE__) . '/../robbiblubber.util.coding/base64.class.php';

use Robbiblubber\Util\Configuration\DdpString;
use Robbiblubber\Util\Coding\Base64;
use PDO;
use Exception;



/** This class represents a database connection. */
class Command
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members                                                                                                  //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Success flag. */
    private $_Success = FALSE;
    
    /** Error message. */
    private $_Msg = NULL;
    
    /** Connection. */
    private $_Cn = NULL;
    
    /** SQL text. */
    private $_Sql = '';
    
    /** Paramters. */
    private $_Params = Array();
    
    /** SQL Statement. */
    private $_Statement = NULL;
    
    /** SQL result. */
    private $_Result = NULL;



    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // constructors                                                                                                     //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Creates a new instance of this class. */
    public function __construct() 
    {
        if(strpos($_GET['handle'], '/') !== FALSE) { $this->_Msg = 'Invalid handle.'; return; }
        
        $hfile = dirname(__FILE__) . '/../../conn/' . $_GET['handle'];
        
        if(!file_exists($hfile))
        {
            $this->_Msg = 'Invalid handle.'; return;
        }        
        touch($hfile);
                
        $ddp = DdpString::fromFile($hfile);
        
        switch($ddp->getValue('provider'))
        {
            case 'sqlite':
                $f = $ddp->getValue('file');
                if(!file_exists($f)) { $this->_Msg = 'Database not found.'; return; }
                
                try
                {
                    $this->_Cn = new PDO('sqlite:' . $f);
                }
                catch(Exception $ex) { $this->_Msg = $ex->getMessage(); return; }

                if(!$this->_Cn) { $this->_Msg = 'Connction failed.'; return; }
                break;
            case 'mysql':
                $cstr = 'mysql:';
                
                if($ddp->getValue('host') != '') { $cstr .= ('host=' . $ddp->getValue('host') . ';'); }
                $cstr .= ('dbname=' . $ddp->getValue('db') . ';');
                
                try 
                {
                    if($ddp->getValue('uid') == '')
                    {
                        $this->_Cn = new PDO($cstr);
                    }
                    else { $this->_Cn = new PDO($cstr, $ddp->getValue('uid'), $ddp->getValue('pwd')); };
                }
                catch (Exception $ex) { $this->_Msg = $ex->getMessage(); return; }
                
                if(!$this->_Cn) { $this->_Msg = 'Connction failed.'; return; }
                break;
            case 'pgsql':
                $cstr = 'pgsql:';
                
                if($ddp->getValue('host') != '') { $cstr .= ('host=' . $ddp->getValue('host') . ';'); }
                if($ddp->getValue('port') != '') { $cstr .= ('host=' . $ddp->getValue('host') . ';'); }
                $cstr .= ('dbname=' . $ddp->getValue('db') . ';');
                if($ddp->getValue('uid') != '') { $cstr .= ('user=' . $ddp->getValue('uid') . ';'); }
                if($ddp->getValue('pwd') != '') { $cstr .= ('password=' . $ddp->getValue('pwd') . ';'); }
                
                try 
                {
                    $this->_Cn = new PDO($cstr);
                }
                catch (Exception $ex) { $this->_Msg = $ex->getMessage(); return; }
                
                if(!$this->_Cn) { $this->_Msg = 'Connction failed.'; return; }
                break;
            default:
                $this->_Msg = 'Unknown provider.'; return;
        }
        
        $this->_Cn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->_Success = TRUE;
        
        if(isset($_GET['sql']))
        {
            $this->_Sql = Base64::toString($_GET['sql']);
        }
        
        if($_GET['hasp'] == "true")
        {
            foreach(split('~', $_GET['p']) as $i)
            {
                array_push($this->_Params, new DbParam($i));
            }
        }
    }
    
    

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public static methods                                                                                            //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    
    /** Returns a new instance.
     * @return Command Connection. */
    public static function initialize()
    {
        return new Command();
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public methods                                                                                                   //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Executes the SQL statement. */
    public function executeSql()
    {
        if(!$this->_Success) return;
        
        try
        {
            $this->_Statement = $this->_Cn->prepare($this->_Sql);

            if($this->_Statement === FALSE)
            {
                $this->dumpErrors();
                return;
            }

            foreach($this->_Params as $i)
            {
                $i->apply($this->_Statement);
            }

            $this->_Statement->execute();
        }
        catch(Exception $ex) { $this->_Success = FALSE; $this->_Msg = $ex->getMessage(); }
    }
    
    
    /** Gets SQL result metadata.
     * @return string Base64-encoded metadata. */
    public function getMeta()
    {
        if(!$this->_Success) { return ''; }
        if($this->getResult() === FALSE) { return ''; }
        
        $rval = '';
        $first = TRUE;
        foreach(array_keys($this->_Result[0]) as $i)
        {
            if(is_numeric($i)) continue;
            if($first)
            {
                $first = FALSE;
            }
            else { $rval .= ':'; }
        
            $rval .= Base64::fromString($i, FALSE);
        }
        
        return $rval;
    }
    
    
    /** Gets SQL result data.
     * @return string Base64-encoded data. */
    public function getData()
    {
        if(!$this->_Success) { return ''; }
        if($this->getResult() === FALSE) { return ''; }
        
        try
        {
            $rval = '';
            $first = TRUE;
            foreach($this->_Result as $i)
            {
                if($first)
                {
                    $first = FALSE;
                }
                else { $rval .= '~'; }

                for($k = 0; $k < $this->_Statement->columnCount(); $k++)
                {
                    if($k != 0) { $rval .= ':'; }

                    $rval .= Base64::fromString($i[$k], FALSE);
                }
            }
        }
        catch(Exception $ex) { $this->_Success = FALSE; $this->_Msg = $ex->getMessage(); }
        
        return $rval;
    }
    
    
    /** Gets a single Bae64-encoded result.
     * @return string Result. */
    public function getSingle()
    {
        try
        {
            if(!$this->_Success) { return ''; }
            $this->_Result = $this->_Statement->fetch();

            if($this->_Result === FALSE) { return ''; }
        }
        catch(Exception $ex) { $this->_Success = FALSE; $this->_Msg = $ex->getMessage(); }
        
        return Base64::fromString($this->_Result[0]);
    }

    
    /** Gets the SQL result.
     * @return array Result set. */
    public function getResult()
    {
        if($this->_Result == NULL) 
        { 
            $this->_Result = $this->_Statement->fetchAll();
        }
        
        return $this->_Result;
    }
    
    
    /** Gets the number of rows effected by the last statement.
     * @return int Number of rows. */
    public function rowCount()
    {
        if(!$this->_Success) { return 0; }
        
        return $this->_Statement->rowCount();
    }


    /** Returns the error message masked for ddp.
     * @return string Message. */
    public function getMessage()
    {
        return DdpString::mask($this->_Msg);
    }
    
    
    /** Gets the open database connection.
     * @return PDO Connection. */
    public function getConnection()
    {
        return $this->_Cn;
    }
    
    
    /** Returns if the connection operation is successful.
     * @return boolean Returns TRUE if the connection is valid, otherwise returns FALSE. */
    public function isSuccessful()
    {
        return $this->_Success;
    }
    
    
    /** Gets the current SQL.
     * @return string SQL. */
    public function getSQL()
    {
        return $this->_Sql;
    }
    
    
    /** Gets the parameter array.
     * @return array Paramters. */
    public function getParameters()
    {
        return $this->_Params;
    }
}

?>