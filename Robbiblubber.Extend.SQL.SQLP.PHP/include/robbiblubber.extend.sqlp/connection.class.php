<?php namespace Robbiblubber\Extend\SQL\SQLP;

require_once dirname(__FILE__) . '/connection.class.php';
require_once dirname(__FILE__) . '/dbparam.class.php';
require_once dirname(__FILE__) . '/../robbiblubber.util/stringop.class.php';
require_once dirname(__FILE__) . '/../robbiblubber.util.coding/sha256.class.php';
require_once dirname(__FILE__) . '/../robbiblubber.util.coding/outputformat.class.php';
require_once dirname(__FILE__) . '/../robbiblubber.util.configuration/ddpstring.class.php';

use Robbiblubber\Util\StringOp;
use Robbiblubber\Util\Coding\SHA256;
use Robbiblubber\Util\Coding\OutputFormat;
use Robbiblubber\Util\Configuration\DdpString;
use PDO;
use Exception;



/** This class represents a database connection. */
class Connection
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members                                                                                                  //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Connection hanlde. */
    private $_Handle;
    
    /** Connection time. */
    private $_Time;
    
    /** Handle exists flag. */
    private $_Exists;
    
    /** Target database. */
    private $_Target = NULL;
    
    /** Provider. */
    private $_Provider = NULL;
    
    /** User name. */
    private $_User = NULL;

    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // constructors                                                                                                     //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Creates a new instance of this class.
     * @param string $handle Connection handle. */
    public function __construct($handle)
    {
        if((strpos($handle, '/') !== FALSE) || (strpos($handle, '.') !== FALSE))
        {
            $handle = '~xxx';
        }
        $this->_Handle = $handle;
        
        if($this->_Exists = file_exists(dirname(__FILE__) . '/../../conn/' . $handle))
        {
            $this->_Time = filemtime(dirname(__FILE__) . '/../../conn/' . $handle);
        }
        else { $this->_Time = 0; }
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public static methods                                                                                            //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Creates a database connection.
     * @return string Returns the connection handle if successful, otherwiese returns an error message prceded by '/'. */
    public static function create()
    {
        $ddp = new DdpString();
        $rval = FALSE;
        $f = '/Connction failed.';
        
        Connection::purge();
        
        switch($_GET['provider'])
        {
            case 'sqlite':
                if(!file_exists($_GET['file'])) { $f = '/Database not found.'; break; }
                if(!Connection::authenticate($_GET['file'] . '.aclf')) { $f = '/Authentication failed.'; break; }
                
                try
                {
                    $cn = new PDO('sqlite:' . $_GET['file']);
                }
                catch(Exception $ex) { return ('/' . $ex->getMessage()); }
                
                if($cn)
                {
                    $ddp->setValue('provider', 'sqlite');
                    $ddp->setValue('file', $_GET['file']);
                    if(isset($_GET['uid'])) { $ddp->setValue('uid', $_GET['uid']); }
                    $rval = TRUE;
                }
                break;
            case 'mysql':
            case 'mariadb':
                $cstr = 'mysql:';
                $host = ''; $uid = ''; $pwd = '';
                
                if((isset($_GET['host'])) && (trim($_GET['host']) != '')) { $cstr .= ('host=' . ($host = $_GET['host']) . ';'); }
                $cstr .= ('dbname=' . $_GET['db'] . ';');
                if((isset($_GET['uid'])) && (trim($_GET['uid']) != '')) { $uid = $_GET['uid']; }
                if((isset($_GET['pwd'])) && (trim($_GET['pwd']) != '')) { $pwd = $_GET['pwd']; }
                
                try
                {
                    if($uid == '')
                    {
                        $cn = new PDO($cstr);
                    }
                    else { $cn = new PDO($cstr, $uid, $pwd); }
                }
                catch(Exception $ex) { return ('/' . $ex->getMessage()); }
                
                if($cn)
                {
                    $ddp->setValue('provider', 'mysql');
                    $ddp->setValue('host', $host);
                    $ddp->setValue('db', $_GET['db']);
                    $ddp->setValue('uid', $uid);
                    $ddp->setValue('pwd', $pwd);
                    
                    $rval = TRUE;
                }
                break;
            case 'pgsql':
                $cstr = 'pgsql:';
                $host = ''; $port = ''; $uid = ''; $pwd = '';
                
                if((isset($_GET['host'])) && (trim($_GET['host']) != '')) { $cstr .= ('host=' . ($host = $_GET['host']) . ';'); }
                if((isset($_GET['port'])) && (trim($_GET['port']) != '')) { $cstr .= ('port=' . ($port = $_GET['port']) . ';'); }
                $cstr .= ('dbname=' . $_GET['db'] . ';');
                if((isset($_GET['uid'])) && (trim($_GET['uid']) != '')) { $cstr .= ('user=' . ($uid = $_GET['uid']) . ';'); }
                if((isset($_GET['pwd'])) && (trim($_GET['pwd']) != '')) { $cstr .= ('password=' . ($pwd = $_GET['pwd']) . ';'); }
                
                try
                {
                    $cn = new PDO($cstr);
                }
                catch(Exception $ex) { return ('/' . $ex->getMessage()); }
                
                if($cn)
                {
                    $ddp->setValue('provider', 'pgsql');
                    $ddp->setValue('host', $host);
                    $ddp->setValue('port', $port);
                    $ddp->setValue('db', $_GET['db']);
                    $ddp->setValue('uid', $uid);
                    $ddp->setValue('pwd', $pwd);
                    
                    $rval = TRUE;
                }
                break;
        }
        
        if($rval)
        {
            $f = StringOp::unique();
            $ddp->save(dirname(__FILE__) . '/../../conn/' . $f);
        }
        
        return $f;
    }
    
    
    /** Authenticates a user.
     * @param string $acl ACL file. If NULL, the superuser authentication file.
     * @return boolean Returns TRUE if successful, otherwiese returns FALSE. */
    public static function authenticate($acl = NULL)
    {
        if($acl == null) { $acl = dirname(__FILE__) . '/../../conf/super.aclf'; }
        
        if(file_exists($acl))
        {
            $ddp = DdpString::fromFile($acl);
            
            if(isset($_GET['hash']))
            {
                $pwd = $_GET['hash'];
            }
            else if(isset($_GET['pwd']))
            {
                $pwd = SHA256::getHash($_GET['pwd'], OutputFormat::HEX);
            }
            else { $pwd = SHA256::getHash('', OutputFormat::HEX); }
            
            return ($ddp->getValue($_GET['uid']) == $pwd);
        }
        
        return TRUE;
    }
        
    
    /** Returns a list of active connections.
     * @return array List of connections. */
    public static function activeConnections()
    {
        $rval = Array();
        
        foreach(scandir(dirname(__FILE__) . '/../../conn') as $i)
        {
            if(strpos($i, '.') !== FALSE) continue;
            array_push($rval, new Connection($i));
        }
        return $rval;
    }
    
    
    /** Gets the connection timeout.
     * @return int Timeout in seconds. */
    public static function getTimeout()
    {
        if(file_exists(dirname(__FILE__) . '/../../conf/connection.timeout'))
        {
            return file_get_contents(dirname(__FILE__) . '/../../conf/connection.timeout');
        }
        
        return 21600;
    }
    
    
    /** Sets the connection timeout.
     * @param int $value New timeout.
     * @return boolean Returns TRUE if successful, otherwise returns FALSE. */
    public static function setTimeout($value)
    {
        if(($value < 3600) || ($value > 2678400)) return FALSE;
        
        file_put_contents(dirname(__FILE__) . '/../../conf/connection.timeout', $value);
        return TRUE;
    }
    
    
    /** Purges connections.
     * @return int Number of connections closed. */
    public static function purge()
    {
        $n = 0;
        $t = (time() - Connection::getTimeout());
        
        foreach(Connection::activeConnections() as $i)
        {
            if($i->getTime() < $t)
            {
                $i->close();
                $n++;
            }
        }
        return $n;
    }
    
    
    /** Returns an instance for a handle.
     * @param string $handle Handle.
     * @return Connection Connection. */
    public static function get($handle = NULL)
    {
        if($handle == NULL) { $handle = $_GET['handle']; }
        return new Connection($handle);
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public methods                                                                                                   //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Gets the connection handle.
     * @return string Handle. */
    public function getHandle()
    {
        return $this->_Handle;
    }
    
    
    /** Gets the last access time for the connection.
     * @return int Timestamp. */
    public function getTime()
    {
        return $this->_Time;
    }
    
    
    /** Gets a value indicating if the connection exists.
     * @return boolean Returns TRUE if the connection exists, otherwise returns FALSE. */
    public function exists()
    {
        return $this->_Exists;
    }
    
    
    /** Gets the target database.
     * @return string Target database. */
    public function getTarget()
    {
        $this->_read();
        return $this->_Target;
    }
    
    
    /** Gets the provider for this connection.
     * @return string Target database. */
    public function getProvider()
    {
        $this->_read();
        return $this->_Provider;
    }
    
    
    /** Gets the provider for this connection.
     * @return string Target database. */
    public function getUser()
    {
        $this->_read();
        return $this->_User;
    }
    
    
    /** Closes a connection. */
    public function close()
    {
        unlink(dirname(__FILE__) . '/../../conn/' . $this->_Handle);
        $this->_Exists = FALSE;
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private methods                                                                                                  //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Reads connection data. */
    private function _read()
    {
        if($this->_Provider == NULL)
        {
            $ddp = DdpString::fromFile(dirname(__FILE__) . '/../../conn/' . $this->_Handle);
            
            $this->_Provider = $ddp->getValue('provider');
            $this->_User = $ddp->getValue('uid');
            if($this->_Provider == 'sqlite')
            {
                $this->_Target = $ddp->getValue('file');
            }
            else 
            {
                $this->_Target = $ddp->getValue('db');
            }
        }
    }
}
?>