<?php namespace Robbiblubber\Extend\SQL\SQLP;

require_once dirname(__FILE__) . '/../robbiblubber.util.configuration/ddpstring.class.php';

use Robbiblubber\Util\Configuration\DdpString;
use PDO;



/** This class implements a database parameter. */
class DbParam
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members                                                                                                  //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** @var string Parameter name. */
    private $_Name;
    
    /** @var mixed Parameter value. */
    private $_Value;
    
    /** @var int Parameter type. */
    private $_Type;
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // constructors                                                                                                     //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Creates a new instance of this class.
     * @param string $s Base64-encoded parameter data. */
    public function __construct($s) 
    {
        $ddp = DdpString::fromString($s, OutputFormat::BASE64);
        
        $this->_Name = $ddp->getValue('name');
        $this->_Type = $ddp->getValue('type');
        
        if($this->_Type == "bool")
        {
            $this->_Value = $ddp->getBoolean('value');
        }
        else if($this->_Type == "null")
        {
            $this->_Value = NULL;
        }
        else
        {
             $this->_Value = $ddp->getValue('value');
        }
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public methods                                                                                                   //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Gets the parameter name.
     * @return string Parameter name. */
    public function getName()
    {
        return $this->_Name;
    }
    
    
    /** Gets the parameter value.
     * @return mixed Parameter value.. */
    public function getValue()
    {
        return $this->_Value;
    }
    
    
    /** Gets the parameter type.
     * @return int Parameter type. */
    public function getType()
    {
        return $this->_Type;
    }
    
    
    /** Applies the parameter to a statement.
     * @param PDOStatement $cmd Statement. */
    public function apply($cmd)
    {
        if($this->_Type == 'null')
        {
            $cmd->bindParam(':a', $this->_Value, PDO::PARAM_NULL);
        }
        else
        {
            $cmd->bindParam(':a', $this->_Value);
        }
    }
}

?>