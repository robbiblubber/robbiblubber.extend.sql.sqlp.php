<?php namespace Robbiblubber\Util\Coding;

 require_once dirname(__FILE__) . '/../robbiblubber.util/stringop.class.php';
 
  

/** This class provides UUencode methods. */
class UUencode
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public static methods                                                                                            //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Returns a UUencoded string for a value.
     * @param mixed $value Value.
     * @return string UUencoded string. */
    public static function toUUencoded($value)
    {
        if(is_array($value))
        {
            return UUencode::fromBytes($value);
        }
        
        return UUencode::fromString($value);
    }
    
    
    /** Returns a UUencoded value for a byte array.
     * @param array $value Byte value.
     * @return string UUencoded expression. */
    public static function fromBytes($value)
    {
        return convert_uuencode(Strings::fromBytes($value));
    }
    
    
    
    /** Returns a UUencoded value for a string.
     * @param array $value Byte value.
     * @return string UUencoded expression. */
    public static function fromString($value)
    {
        return convert_uuencode($value);
    }


    /** Returns a string representation for a UUencoded expression.
    * @param string $value UUencoded expression.
    * @return string String. */
    public static function toString($value)
    {
        return convert_uudecode($value);
    }

    
    /** Converts a UUencoded string to a byte array.
    * @param string $value UUencoded string.
    * @return array Byte array. */
    public static function toBytes($value)
    {
        return Strings::toBytes(convert_uudecode($value));
    }
}

?>