<?php namespace Robbiblubber\Util\Coding;


/** This class defines output format constants. */
class OutputFormat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public constants                                                                                                 //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Plain format. */
    const PLAIN = 0;
    
    /** Base64 format. */
    const BASE64 = 1;
    
    /** Hexadecimal format. */
    const HEX = 2;
    
    /** UUencode format. */
    const UUENCODE = 4;
}

?>