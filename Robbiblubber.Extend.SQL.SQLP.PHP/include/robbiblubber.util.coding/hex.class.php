<?php namespace Robbiblubber\Util\Coding;


/** This class provides hexadecimal conversion methods. */
class Hex
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public static methods                                                                                            //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Returns a hexadecimal expression for a value.
     * @param mixed $value Value.
     * @return string Hexadecimal expression. */
    public static function toHex($value)
    {
        if(is_array($value))
        {
            return Hex::fromBytes($value);
        }
        
        if(is_int($value))
        {
            return Hex::fromByte($value);
        }
        
        return Hex::fromString($value);
    }
    
    
    /** Returns a 2-digit hexadecimal value for a byte.
     * @param int $value Byte value.
     * @return string Hexadecimal expression. */
    public static function fromByte($value)
    {
        $rval = dechex($value);
        if(strlen($rval) == 1) { $rval = ('0' . $rval); }

        return $rval;
    }
    
    
    /** Returns a hexadecimal value for a byte array.
     * @param array $value Byte value.
     * @return string Hexadecimal expression. */
    public static function fromBytes($value)
    {
        $rval = '';

        foreach($value as $i)
        {
            $rval .= Hex::fromByte($i);
        }
        return $rval;
    }
    
    
    
    /** Returns a hexadecimal value for a string.
     * @param array $value Byte value.
     * @return string Hexadecimal expression. */
    public static function fromString($value)
    {
        return bin2hex($value);
    }


    /** Returns a string representation for a hexadecimal expression.
    * @param string $value Hexadecicmal expression.
    * @return string String. */
    public static function toString($value)
    {
        return hex2bin($value);
    }

    
    /** Converts a hexadecimal string to a byte array.
    * @param string $value Hexadecimal string.
    * @return array Byte array. */
    public static function toBytes($value)
    {
        $rval = array();

        for($i = 0; $i < strlen($value); $i += 2)
        {
            $rval[$i / 2] = hexdec(substr($value, $i, 2));
        }        
        return $rval;
    }
}

?>