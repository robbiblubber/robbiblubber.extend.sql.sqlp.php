<?php namespace Robbiblubber\Util\Coding;

 require_once dirname(__FILE__) . '/../robbiblubber.util/stringop.class.php';
 
 

/** This class provides Base64 encoding methods. */
class Base64
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public static methods                                                                                            //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Returns a Base64-encoded string for a value.
     * @param mixed $value Value.
     * @param boolean $pad Add padding characters.
     * @return string Base64-encoded string. */
    public static function toBase64($value, $pad = TRUE)
    {
        $rval = '';
        
        if(is_array($value))
        {
            $rval = Base64::fromBytes($value);
        }
        else if(is_int($value))
        {
            $rval = Base64::fromByte($value);
        }
        
        if(!$pad)
        {
            $rval = str_replace('=', '', $rval);
        }
        
        return $rval;
    }
    
    
    /** Returns a Base64-encoded string for a byte array.
     * @param array $value Byte value.
     * @param boolean $pad Add padding characters.
     * @return string Base64-encoded string. */
    public static function fromBytes($value, $pad = TRUE)
    {
        $rval = base64_encode(Strings::fromBytes($value));
        
        if(!$pad)
        {
            $rval = str_replace('=', '', $rval);
        }
        
        return $rval;
    }
    
    
    
    /** Returns a Base64-encoded string for a string.
     * @param array $value Byte value.
     * @param boolean $pad Add padding characters.
     * @return string Base64-encoded string. */
    public static function fromString($value, $pad = TRUE)
    {
        $rval = base64_encode($value);
        
        if(!$pad)
        {
            $rval = str_replace('=', '', $rval);
        }
        
        return $rval;
    }


    /** Returns a string for a Base64-encoded string.
    * @param string $value Base64-encoded string.
    * @return string String. */
    public static function toString($value)
    {
        while((strlen($value) % 4) != 0)
        {
            $value .= '=';
        }
        
        return base64_decode($value);
    }

    
    /** Converts a Base64-encoded string to a byte array.
    * @param string $value Base64-encoded string.
    * @return array Byte array. */
    public static function toBytes($value)
    {
        while((strlen($value) % 4) != 0)
        {
            $value .= '=';
        }
        
        return Strings::toBytes(base64_decode($value));
    }
}

?>