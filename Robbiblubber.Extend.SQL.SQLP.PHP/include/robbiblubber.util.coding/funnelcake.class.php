<?php namespace Robbiblubber\Util\Coding;

require_once dirname(__FILE__) . '/outputformat.class.php';
require_once dirname(__FILE__) . '/sha256.class.php';
require_once dirname(__FILE__) . '/../robbiblubber.util/stringop.class.php';
require_once dirname(__FILE__) . '/hex.class.php';
require_once dirname(__FILE__) . '/base64.class.php';
require_once dirname(__FILE__) . '/uuencode.class.php';



/** This class implements the FunnelCake algorithm.
    FunnelCake provides sufficiently secure symmetric encryption that can easily be implemented on different platforms. */
class FunnelCake
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private static members                                                                                           //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Seed. */
    private static $_SEED = 'ZDQwZGI1YmFhNDVlNTM5ZTRmM2JiYjU4ZGQ1OGNjZDJkZDNiMjJiNThlYTBmYjE4MjdlOWNkNGFhM2U3Zjk5NQ';
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public static methods                                                                                            //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Encrypts a string.
     * @param string $value String.
     * @param string $secret Shared secret.
     * @param int $option Format option. 
     * @return string Encrypted string. */
    public static function encrypt($value, $secret, $option = OutputFormat::HEX)
    {
        $key = '';
        $seed =  FunnelCake::$_SEED;
                
        while(strlen($key) < strlen($value))
        {
            $seed = FunnelCake::_getHash($secret . $seed);
            $key .= $seed;
        }
        $key = substr($key, 0, strlen($value));
        
        echo '(' . $key . ')';
            
        $v = Strings::toBytes($value);
        $k = Strings::toBytes($key);
        
        for($i = 0; $i < count($v); $i++)
        {
            $v[$i] = ($v[$i] ^ $k[$i]);
        }
        
        $rval = '';
        switch($option)
        {
            case OutputFormat::BASE64:
                $rval = Base64::fromBytes($v);
                break;
            case OutputFormat::PLAIN:
                $rval = Strings::fromBytes($v);
                break;
            case OutputFormat::UUENCODE:
                $rval = UUencode::fromBytes($v);
                break;
            default:
                $rval = Hex::fromBytes($v);
        }
                
        return $rval;
    }
    
    
    /** Decrypts a string.
     * @param string $value String.
     * @param string $secret Shared secret.
     * @param int $option Format option.
     * @return string Decrypted string. */
    public static function decrypt($value, $secret, $option = OutputFormat::HEX)
    {
        $v = '';
        switch($option)
        {
            case OutputFormat::BASE64:
                $v = Base64::toBytes($value);
                break;
            case OutputFormat::PLAIN:
                $v = Strings::toBytes($value);
                break;
            case OutputFormat::UUENCODE:
                $v = UUencode::toBytes($value);
                break;
            default:
                $v = Hex::toBytes($value);
        }
        
        $key = '';
        $seed =  FunnelCake::$_SEED;
                
        while(strlen($key) < count($v))
        {
            $seed = FunnelCake::_getHash($secret . $seed);
            $key .= $seed;
        }
        $key = substr($key, 0, count($v));
        
        $k = Strings::toBytes($key);
        
        for($i = 0; $i < count($v); $i++)
        {
            $v[$i] = ($v[$i] ^ $k[$i]);
        }
        
        return Strings::fromBytes($v);
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private static methods                                                                                           //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Gets a Base64-encoded hash value.
     * @param string $value String.
     * @return string Hash. */
    private static function _getHash($value)
    {
        return str_replace('=', '', SHA256::getHash($value, OutputFormat::BASE64));
    }
}

?>