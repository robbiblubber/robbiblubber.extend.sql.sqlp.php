<?php namespace Robbiblubber\Util\Coding;

require_once dirname(__FILE__) . '/outputformat.class.php';
require_once dirname(__FILE__) . '/base64.class.php';
require_once dirname(__FILE__) . '/uuencode.class.php';



/** This class implements the SHA256 algorithm. */
class SHA256
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public static methods                                                                                            //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Returns the hash value for a string.
     * @param string $value String.
     * @param string $option Format option. */
    public static function getHash($value, $option = OutputFormat::HEX)
    {
        switch($option)
        {
            case OutputFormat::PLAIN:
                return hash('SHA256', $value, TRUE);
            case OutputFormat::BASE64:
                return Base64::fromString(hash('SHA256', $value, TRUE));
            case OutputFormat::UUENCODE:
                return UUencode::fromString(hash('SHA256', $value, TRUE));
            default:
                return hash('SHA256', $value);
        }
    }
}

?>