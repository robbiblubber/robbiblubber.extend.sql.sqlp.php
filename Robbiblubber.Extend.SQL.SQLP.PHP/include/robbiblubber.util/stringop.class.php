<?php namespace Robbiblubber\Util;


/** This class provides string manipulation and utility methods. */
class StringOp
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // constants                                                                                                        //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Letter constant. */
    const _ALPHA = "Aa9Bb8Cc7Dd6Ee5Ff4Gg3Hh2Ii1Jj0KkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public static methods                                                                                            //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /** Converts a string to a byte array.
     * @param string $value String.
     * @return array Byte array. */
    /* Somehow unpack('C*', $value) returns an array starting with index 1,
     * str2barray returns an array starting with index 0. */
    public static function toBytes($value)
    {
        $rval = array();

        for($i = 0; $i < strlen($value); $i++)
        {
            $rval[$i] = ord(substr($value, $i, 1));
        }
        return $rval;
    }
    
    
    
    /** Converts a byte array to a string.
     * @param array $value Byte array.
     * @return string String. */
    public static function fromBytes($value)
    {
        $rval = '';

        foreach($value as $i)
        {
            $rval .= chr($i);
        }
        return $rval;
    }
    
    
    /** Returns a random alphanumeric string of a given length.
     * @param int $length Random string length. 
     * @return string Random string. */
    public static function random($length = 24)
    {
        $rval = '';
            
        for($i = 0; $i < $length; $i++)
        {
            $rval .= substr(StringOp::_ALPHA, rand(0, 61), 1);
        }

        return $rval;
    }
    
    
    /** Returns a unique random alphanumeric string of a given length.
     * @param int $length String length. 
     * @return string Unique string. */
    public static function unique($length = 24)
    {
        return substr(StringOp::random(3) . StringOp::toAlpha(microtime(TRUE) * 10000) . StringOp::random($length), 0, $length);
    }
    
    
    /** Converts an integer to an alphanumeric string.
     * @param int $value Integer.
     * @return string String. */
    public static function toAlpha($value)
    {
        $rval = "";        
        
        if($value < 0) 
        { 
            $rval = '-'; 
            $value = -$value;
        }
        
        while($value > 0)
        {
            $rval .= substr(StringOp::_ALPHA, ($value % 62), 1);
            $value = ((int) ($value / 62));
        }
        
        return $rval;
    }
}

?>