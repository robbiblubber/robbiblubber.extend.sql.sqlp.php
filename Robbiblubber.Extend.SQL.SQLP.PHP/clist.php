<?php namespace Robbiblubber\Extend\SQL\SQLP;

require_once dirname(__FILE__) . '/include/robbiblubber.extend.sql.sqlp/connection.class.php';
require_once dirname(__FILE__) . '/include/robbiblubber.util.configuration/ddpstring.class.php';

use Robbiblubber\Util\Configuration\DdpString;



header('Content-Type: text/plain');



if(!Connection::authenticate())
{
    echo('success=false;message=Authentication failed.;');
}
else
{
     $connections = Connection::activeConnections();
     
    echo('success=true;count=' . count($connections) . ';');
    
    $showtime = TRUE;
    $showtar  = TRUE;
    $showprov = FALSE;
    $showuser = FALSE;
    $handle = $time = $tar = $prov = $user = '';
    
    if(isset($_GET['showtime'])) { $showtime = ($_GET['showtime'] == 'true'); }
    if(isset($_GET['showtar']))  { $showtar  = ($_GET['showtar']  == 'true'); }
    if(isset($_GET['showprov'])) { $showprov = ($_GET['showprov'] == 'true'); }
    if(isset($_GET['showuser'])) { $showuser = ($_GET['showuser'] == 'true'); }
    
    for($i = 0; $i < count($connections); $i++)
    {
        $handle .= ((($i == 0) ? 'handle[]=' : ',') . $connections[$i]->getHandle());
        if($showtime) { $time .= ((($i == 0) ? 'time{]=' : ',') . $connections[$i]->getTime()); }
        if($showtar)  { $tar  .= ((($i == 0) ? 'tar{[='  : ',') . DdpString::mask($connections[$i]->getTarget())); }
        if($showprov) { $prov .= ((($i == 0) ? 'prov{]=' : ',') . DdpString::mask($connections[$i]->getProvider())); }
        if($showuser) { $user .= ((($i == 0) ? 'user[]=' : ',') . DdpString::mask($connections[$i]->getUser())); }
    }
    
    echo($handle . ';');
    if($showtime) { echo($time . ';'); }
    if($showtar)  { echo($tar  . ';'); }
    if($showprov) { echo($prov . ';'); }
    if($showuser) { echo($user . ';'); }
 }
?>